# Quarto templates for R

## Templates

I here collect a number of templates to use for the various report, presentations, etc I produce in different projects.


## Installing

To install directly from the GitLab repository:

```bash
quarto add https://gitlab.com/ScientiaFelis/r_quartotemplates/-/archive/main/r_quartotemplates-main.zip
```

This will install the extension and you can use the template.qmd file here as a starting place for your report.

## Using

The reports are using a number of latex template partials to configure e.g. coverpages, title pages and special fact pages, that some agencies require, to produce a report that conforms to the project requirements.

As of today there is:

**PoMS Report**: setting pomreport-pdf as format in YAML


## Format Options

**pomsreport-pdf**

Have lots of options and the ones set by default are:

```md     
      Stitle: #shorttitle
      toc: true
      toc-depth: 3
      toc-title: Innehållsförteckning
      number-sections: true
      number-depth: 3
      lof: true
      lot: true
      fig-pos: H
      papersize: a4
      logo1: images/NVV.png  # logo on left upper corner of faktasida
      logo2: images/LU.png # optional logo to left upper corner of title page
      collectiondate: "2022" # Year / date of collection
      cover-bg-image: "cover.png" # Cover image
      cover-page-color: "FFFFFF"  # provide hex color code
      cover-text-color: "875E29"  # provide hex color code or defined colour
      arendenr: 123456678 # Ärendenummer
      avtalnr: 87654321 # Avtalsnummer
      bibliography: references.bib # Path to bibliography files
      csl: sage-harvard.csl      # Path to reference style file
      crossref:
        fig-title: Figur
        fig-prefix: Figur
        tbl-title: Tabell
        tbl-prefix: Tabell
        lof-title: "Figurindex"
        lot-title: "Tabellindex"
      execute:
        echo: false
        warning: false
        message: false
```
The abstract can be written in the text under the heading ```# Abstract```  and optionally another language, such as ```# Sammanfattning```. These need to be specified in the YAML as section-identifiers, se below, under the abstract-section: option. It will automatically be set as metadata as if it were written in the YAML head and be set where it is referred. In this report on the fact page and Title page.

## Example YAML



```md
title: "A longer nice title"
subtitle: "A subtitle"
Stitle: "A shorter version of the title" #A short title for the heading
author:
  - name: First and second names
    affiliations:
      - id: lu
        name: "Lunds Universitet"
        department: "Biodiversitet, Biologiska institutionen"
        address:
        city: Lund
  - name: Secon author names
    affiliations:
      - ref: lu # If this author has the same affiliation it only have to reference the id of the first
  - name: Third author names
    affiliations:
      - ref: lu
date: today
date-format: "D MMMM YYYY"
collectiondate: "2022" #Date of data collection
arendenr: "NV-9800-00"
avtalsnr: "000-01-002"
logo1: NVV.png  # logo on left side of fact page
logo2: LU.png  # logo on right side of title page
### Comment or remove the following two lines if NO references are used
bibliography: references.bib # Path to bibliography files 
csl: sage-harvard.csl      # Path to reference style file
### Settings for rendering the document:
format: 
  pomsreport-pdf:
    latex-tinytex: false
    keep-tex: true
    documentclass: scrreprt
    geometry:   
      - top=25mm     # margin settings
      - bottom=35mm
      - left=25mm
      - right=25mm
      - heightrounded
    # Options for front cover
    cover-bg-image: "cover.png"
    cover-page-color: "FFFFFF"  # provide hex color code
    cover-text-color: "875E29"  # provide hex color code or defined colour
    isbn1: 978-91-8039-243-3 #Tryckt isbn
    isbn2: 978-91-8039-244-0 # PDF isbn
    # Options for table of content
    colorlinks: true
# Code options:
highlight-style: arrow # default
fig-align: center
editor: source
# This is the abstract section where you put the abstracts section name in English and another languge.
abstract-section:
  section-identifiers:
    - abstract
    - sammanfattning

```
